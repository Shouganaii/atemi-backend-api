import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

import { Document } from 'mongoose';

@Schema()
class Contacts {
  @Prop()
  _id?: number;

  @Prop()
  email?: string;

  @Prop()
  phone?: string;

  @Prop()
  name?: string;
}
export const ContactsSchema = SchemaFactory.createForClass(Contacts);
class Geoloc {
  @Prop()
  lat: number;

  @Prop()
  lon: number;
}

class Address {
  @Prop()
  street: string;

  @Prop()
  neighbourhood: string;

  @Prop()
  zipCode: string;

  @Prop()
  publicPlace: string;

  @Prop()
  addressNumber: string;

  @Prop()
  location: string;

  @Prop()
  uf: string;

  @Prop()
  geoloc: Geoloc;
}

@Schema()
export class Users extends Document {
  @Prop()
  firstName: string;

  @Prop()
  userAlias: string;

  @Prop()
  lastName: string;

  @Prop()
  email: string;

  @Prop()
  phoneNumber: string;

  @Prop()
  password: string;

  @Prop()
  cpf: string;

  @Prop()
  isActive: boolean;

  @Prop()
  roles: [string];

  @Prop({ type: [ContactsSchema], required: false })
  contacts: Contacts[];

  @Prop(Address)
  address: Address;

  @Prop()
  createdAt: Date;
}
export type UsersDocument = Users & Document;

export type ContactsDocument = Contacts & Document;

export const UsersSchema = SchemaFactory.createForClass(Users);
