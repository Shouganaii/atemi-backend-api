import { Prop } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional } from 'class-validator';

export class Contacts {
  @Prop()
  @ApiProperty()
  _id: string;

  @Prop()
  @ApiProperty()
  email: string;

  @Prop()
  @ApiProperty()
  phone: string;

  @Prop()
  @ApiProperty()
  name: string;
}
export class CreateContactsDTO {
  @Prop()
  @ApiProperty()
  @IsOptional()
  @IsNotEmpty({ message: 'The email is empty' })
  email: string;

  @Prop()
  @IsOptional()
  @IsNotEmpty({ message: 'The phone is empty' })
  phone: string;

  @Prop()
  @ApiProperty()
  @IsNotEmpty({ message: 'The name is empty' })
  name: string;
}
export class ReturnCreateContacts {
  @Prop()
  @ApiProperty()
  _id: string;

  @Prop()
  @ApiProperty({ type: [Contacts] })
  contacts: [Contacts];
}
export class SearchAllContacts {
  @Prop()
  _id: string;

  @Prop()
  lastName: string;

  @Prop()
  firstName: string;

  @Prop()
  contacts: Contacts[];
}
