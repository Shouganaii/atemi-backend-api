import {
  BadRequestException,
  CACHE_MANAGER,
  Inject,
  Injectable,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Schedules, SchedulesDocument } from 'src/schemas/schedule.schema';
import { CreateScheduleDto } from './dto/create-schedule.dto';
import { UpdateScheduleDto } from './dto/update-schedule.dto';
import * as moment from 'moment';
import { isEmpty } from 'lodash';
import { isDate, isBefore } from 'date-fns';
import { ScheduleRepository } from './entities/schedule.interfaces';
import { Cache } from 'cache-manager';
import { Users, UsersDocument } from 'src/schemas/user.schema';
moment.locale('pt-br');

//TODO tipar isso aqui
//TODO melhorar as classes de tratamento de data
@Injectable()
export class SchedulesService implements ScheduleRepository {
  constructor(
    @InjectModel(Schedules.name)
    private scheduleModel: Model<SchedulesDocument>,
    @InjectModel(Users.name)
    private usersModel: Model<UsersDocument>,
    @Inject(CACHE_MANAGER) private cacheManager: Cache,
  ) {}

  async create(data: CreateScheduleDto): Promise<SchedulesDocument | void> {
    const user = await this.usersModel.findById(data.user_id);

    if (isEmpty(user)) throw new BadRequestException('Usuário não encontrado');

    const hasOtherSchedulement = await this.findSchedulesByUserAndTime(
      data.user_id,
      data.scheduled_to,
    );

    if (hasOtherSchedulement)
      throw new BadRequestException('Conflito de horários');

    const contactValidation: number[] = [];

    //TODO improve this section
    if (!isEmpty(data.contacts)) {
      if (isEmpty(user.contacts)) {
        throw new BadRequestException('Usuário não possui contatos');
      }
      //TODO the number here is due to parsing, remove once the swagger updates
      data.contacts.forEach(async (contact_id) => {
        user.contacts.forEach((contact) => {
          if (contact._id == Number(contact_id)) {
            contactValidation.push(Number(contact_id));
          }
        });
      });
    }

    delete data.contacts;

    const schedule = await this.scheduleModel.create(data);

    if (!isEmpty(contactValidation)) {
      schedule.contacts = contactValidation;
      await schedule.save();
    }

    await this.cacheManager.set(`schedules`, JSON.stringify(schedule));

    return schedule;
  }

  async findAll() {
    const schedules = await this.scheduleModel.find();

    if (isEmpty(schedules))
      throw new BadRequestException('Não há agendamentos');

    return schedules;
  }

  async findOne(_id: string): Promise<any> {
    const schedules = await this.findAllSchedulesByUser(_id);

    if (isEmpty(schedules))
      throw new BadRequestException('Não há agendamentos');

    return schedules;
  }

  async postponeSchedule(
    user_id: string,
    schedule_id: string,
    postpone_to: Date,
  ) {
    const schedule = await this.scheduleModel.findOne({
      _id: schedule_id,
      user_id,
    });

    if (!schedule)
      throw new BadRequestException('Não há agendamentos para esse usuário');

    const postponedSchedule = await this.scheduleModel.findByIdAndUpdate(
      { _id: schedule_id },
      {
        was_postponed: true,
        scheduled_to: postpone_to,
      },
      {
        returnOriginal: false,
      },
    );
    //TODO não pode deixar agendar pra dias anteriores

    return postponedSchedule;
  }

  async cancelSchedule(user_id: string, schedule_id: string) {
    const schedule = await this.scheduleModel.findOne({
      _id: schedule_id,
      user_id,
    });

    if (!schedule)
      throw new BadRequestException('Não há agendamentos para esse usuário');
    //TODO implementar data limite para cancelar
    const canceledSchedule = await this.scheduleModel.findByIdAndUpdate(
      { _id: schedule_id },
      {
        is_canceled: true,
        canceled_on: new Date(),
      },
      {
        returnOriginal: false,
      },
    );

    return canceledSchedule;
  }

  async postpone(
    schedule_id: string,
    user_id: string,
    updateScheduleDto: UpdateScheduleDto,
  ) {
    const schedule = await this.findSchedulesByUser(user_id);

    if (isEmpty(schedule))
      return new BadRequestException('Não há agendamentos para esse usuário');

    return await this.scheduleModel.findByIdAndUpdate(
      { _id: schedule_id },
      {
        was_postponed: true,
        scheduled_to: updateScheduleDto.postpone_to,
      },
      {
        returnOriginal: false,
      },
    );
  }

  async cancel(user_id: string, schedule_id: string) {
    const schedule = await this.findSchedulesByUser(user_id);

    if (isEmpty(schedule))
      throw new BadRequestException('Não há agendamentos para esse usuário');

    //TODO levar em conta o limit_date_to_cancel
    return await this.scheduleModel.findByIdAndUpdate(
      { _id: schedule_id },
      {
        is_canceled: true,
        canceled_on: new Date(),
      },
      {
        returnOriginal: false,
      },
    );
  }

  async remove(id: number) {
    return `This action removes a #${id} schedule`;
  }

  async findSchedulesByUserAndTime(user_id: string, scheduled_to: Date) {
    return await this.scheduleModel.findOne({ user_id, scheduled_to });
  }
  async findSchedulesByUser(user_id: string) {
    return await this.scheduleModel.findOne({ user_id });
  }
  async findAllSchedulesByUser(user_id: string) {
    return await this.scheduleModel.find({ user_id });
  }

  transformDates(schedules: any) {
    const schedulesModified = [];

    const schedulesLength = schedules.length;

    for (let index = 0; index < schedulesLength; index++) {
      let {
        notify_at,
        limit_date_to_cancel,
        scheduled_to,
        createdAt,
        updatedAt,
      } = schedules[index];

      const dateParser = new DateParser();

      notify_at = dateParser.toLocalTimezone(notify_at);
      limit_date_to_cancel = dateParser.toLocalTimezone(limit_date_to_cancel);
      scheduled_to = dateParser.toLocalTimezone(scheduled_to);
      createdAt = dateParser.toLocalTimezone(createdAt);
      updatedAt = dateParser.toLocalTimezone(updatedAt);

      const times = {
        notify_at,
        limit_date_to_cancel,
        scheduled_to,
        updatedAt,
        createdAt,
      };

      const modified = { ...schedules[index]._doc, ...times };
      schedulesModified.push(modified);
    }
    return schedulesModified;
  }
}

class DateParser {
  parse(date: any) {
    if (!isDate(date)) throw new Error('A data não está no formato correto!');
    return true;
  }
  timeDistance = (time) => {
    return isBefore(new Date(time), new Date());
  };

  toLocalTimezone = (time) => {
    return moment(time).format('YYYY-MM-DDTHH:mm:ss');
  };

  minus3 = (time) => {
    return moment(time).format('DD-MM-YYYY');
  };
}
