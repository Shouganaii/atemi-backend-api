import { Injectable } from '@nestjs/common';
import { Md5 } from 'ts-md5/dist/md5';

@Injectable()
export class Md5Crypto {
  incrypt(value: any) {
    return Md5.hashStr(value);
  }
}
