import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  BadRequestException,
  UseGuards,
  Req,
  Request,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto, UpdateUserDto } from './interfaces';
import {
  ApiNotFoundResponse,
  ApiBadRequestResponse,
  ApiInternalServerErrorResponse,
  ApiOkResponse,
  ApiTags,
} from '@nestjs/swagger';
import { DetailedException } from 'src/shared/exceptions';
import { AuthenticateUser } from './interfaces/authenticate-user.dto';
import { Token } from 'src/models/Token';
import { User } from './entities/user.entity';
import { JwtAuthGuard } from 'src/strategies/auth-guard';
import { JwtService } from '@nestjs/jwt';
import { AdminGuard } from 'src/shared/provider-guard.guard';
import { RecoverPassword } from './interfaces/recover-password.dto';
@Controller('users')
@ApiTags('users')
@ApiNotFoundResponse({ description: 'Not Found', type: DetailedException })
@ApiBadRequestResponse({ description: 'Bad Request', type: DetailedException })
@ApiInternalServerErrorResponse({
  description: 'Internal Server Error',
  type: DetailedException,
})
export class UsersController {
  constructor(
    private readonly usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  @Post()
  @ApiOkResponse({ type: Token })
  async create(
    @Body() createUserDto: CreateUserDto,
  ): Promise<Token | BadRequestException> {
    return await this.usersService.register(createUserDto);
  }

  @Post('/auth')
  @ApiOkResponse({ type: Token })
  authenticate(@Body() userLogin: AuthenticateUser): Promise<Token> {
    return this.usersService.login(userLogin);
  }

  @Get()
  @ApiOkResponse({ type: [User] })
  @UseGuards(AdminGuard)
  findAll() {
    return this.usersService.findAll();
  }

  @Get(':id')
  @UseGuards(JwtAuthGuard)
  findOne(@Param('id') id: string) {
    return this.usersService.findOne(id);
  }

  @Get('/info/me')
  @UseGuards(JwtAuthGuard)
  @ApiOkResponse({ type: User })
  me(@Req() req: Request) {
    const [, token] = req.headers['authorization'].split(' ');
    const user: any = this.jwtService.decode(token);
    return this.usersService.findOne(user.payload._id as string);
  }

  @Patch(':id')
  @UseGuards(JwtAuthGuard)
  update(@Param('id') id: string, @Body() updateUserDto: UpdateUserDto) {
    return this.usersService.update(id, updateUserDto);
  }

  @Delete(':id')
  @UseGuards(JwtAuthGuard)
  remove(@Param('id') id: string) {
    return this.usersService.remove(id);
  }

  @Post('/recover')
  recover(@Body() body: RecoverPassword) {
    return this.usersService.recoverPassword(body);
  }

  @Post('/recover/:hash')
  recoverWithHash(@Param('hash') hash: string) {
    return this.usersService.validateRecover(hash);
  }
}
