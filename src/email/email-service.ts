import { Injectable } from '@nestjs/common';

@Injectable()
export class EmailService {
  async sendMail(to: string, from: string): Promise<void> {
    console.log('email sent to', to);
  }
}
