import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Mongoose } from 'mongoose';

export type TokensDocument = Tokens & Document;

@Schema({ timestamps: true })
export class Tokens {
  @Prop()
  hash: string;

  @Prop()
  user_id: string;

  @Prop()
  operationType: string;

  @Prop()
  isValid: boolean;
}

export const TokensSchema = SchemaFactory.createForClass(Tokens);
