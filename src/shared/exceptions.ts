import { ApiProperty } from '@nestjs/swagger';

export class DetailedException {
  @ApiProperty()
  statusCode: number;
  @ApiProperty()
  message: string;
  @ApiProperty()
  error: string;
}
