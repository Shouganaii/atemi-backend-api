import { ApiProperty } from '@nestjs/swagger';
import { User } from 'src/users/entities/user.entity';

export class Token {
  constructor(payload: User, access_token: string) {
    this.payload = payload;
    this.access_token = access_token;
  }
  @ApiProperty({ type: User })
  payload: User;
  @ApiProperty()
  access_token: string;
}
