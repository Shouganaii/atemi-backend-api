import { ApiProperty } from '@nestjs/swagger';

export class UpdateScheduleDto {
  @ApiProperty()
  postpone_to: Date;
}
