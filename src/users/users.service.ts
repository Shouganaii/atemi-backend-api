import {
  BadRequestException,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { JwtService } from '@nestjs/jwt';
import { InjectModel } from '@nestjs/mongoose';
import { isEmpty } from 'class-validator';
import { Model } from 'mongoose';
import { Token } from 'src/models/Token';
import { Tokens, TokensDocument } from 'src/schemas/tokens.schema';
import { Users, UsersDocument } from 'src/schemas/user.schema';
import { Md5Crypto } from 'src/shared/crypto';
import { AuthenticateUser } from './interfaces/authenticate-user.dto';
import { CreateUserDto } from './interfaces/create-user.dto';
import { RecoverPassword } from './interfaces/recover-password.dto';
import { UpdateUserDto } from './interfaces/update-user.dto';

@Injectable()
export class UsersService {
  constructor(
    @InjectModel(Users.name) private userModel: Model<UsersDocument>,
    @InjectModel(Tokens.name) private tokensModel: Model<TokensDocument>,
    private jwtService: JwtService,
    private md5Crypto: Md5Crypto,
    private eventEmitter: EventEmitter2,
  ) {}

  async register(
    createUserDto: CreateUserDto,
  ): Promise<Token | BadRequestException> {
    const isEmailInUse = await this.findByEmail(createUserDto.email);

    if (isEmailInUse) throw new BadRequestException('Email in uso!');

    const isAliasInUse = await this.findByAlias(createUserDto.userAlias);

    if (isAliasInUse) throw new BadRequestException('Apelido in uso!');

    const hashedPassword = this.md5Crypto.incrypt(createUserDto.password);

    createUserDto.password = hashedPassword;

    const user = await this.create(createUserDto);

    delete user._doc.password;

    this.eventEmitter.emit('user.auth.register', user);

    return new Token(user, this.jwtService.sign({ payload: user }));
  }

  async create(createUserDto: CreateUserDto): Promise<any> {
    const createdUser = await this.userModel.create(createUserDto);
    return createdUser;
  }

  async login(login: AuthenticateUser): Promise<Token> {
    const isEmail = login.user.includes('@');

    let user: any;

    if (isEmail) {
      const result = await this.userModel.findOne({
        where: { email: login.user },
      });
      user = result;
    } else {
      const result = await this.userModel.findOne({
        where: { userAlias: login.user },
      });
      user = result;
    }

    if (!user) throw new BadRequestException('Usuário não encontrado!');

    if (user.password !== this.md5Crypto.incrypt(login.password)) {
      throw new UnauthorizedException(
        'Credenciais de acesso estão incorretas!',
      );
    }

    if (!user.isActive) {
      this.eventEmitter.emit('user.auth.inactive', user);
      throw new UnauthorizedException('Usuário não ativo!');
    }

    delete user._doc.password;
    this.eventEmitter.emit('user.auth.success', user);
    return new Token(user, this.jwtService.sign({ payload: user }));
  }

  findAll() {
    return this.userModel.find().select({ password: 0 });
  }

  async findByEmail(email) {
    return await this.userModel.findOne({ email });
  }

  async findByAlias(userAlias) {
    return await this.userModel.findOne({ userAlias });
  }

  findOne(id: string) {
    return this.userModel.findById(id).select({ password: 0 });
  }

  update(id: string, updateUserDto: UpdateUserDto) {
    return this.userModel
      .findOneAndUpdate({ _id: id }, { ...updateUserDto }, { new: true })
      .select({ password: 0 });
  }

  remove(id: string) {
    return this.userModel.deleteOne({ _id: id });
  }

  //TODO tipar esse método
  async recoverPassword(userData: RecoverPassword) {
    const isEmail = userData.email.includes('@');

    let user: any;

    if (isEmail) {
      const result = await this.userModel.findOne({
        where: { email: userData },
      });
      user = result;
    } else {
      const result = await this.userModel.findOne({
        where: { userAlias: userData },
      });
      user = result;
    }

    //TODO não deixar escapar a info de que o user é cadastrado ou não
    if (isEmpty(user)) throw new BadRequestException('User does not exist');

    const random = Math.floor(Math.random() * 5);
    const hash = this.md5Crypto.incrypt(random.toString());

    console.log('hash para recuperação:', hash);

    await this.tokensModel.create({
      user_id: user._id,
      hash,
      operationType: 'password_recover',
      isValid: true,
    });

    // arrumar um módulo de email
    // mandar hash via email

    return {
      statusCode: 200,
      message: 'E-mail de recuperação de senha enviado!',
    };
  }

  //TODO tipar esse método
  //TODO adicionar tempo de expiração pro token
  async validateRecover(hash: string) {
    const token = await this.tokensModel.findOne({ hash });

    if (isEmpty(token)) throw new BadRequestException('Token inválido');

    if (!token.isValid) throw new BadRequestException('Token usado');

    token.isValid = false;

    await token.save();

    return {
      statusCode: 200,
      message: 'Troca de senha permitida',
    };
  }
}
