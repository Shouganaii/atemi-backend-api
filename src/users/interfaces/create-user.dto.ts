import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty } from 'class-validator';

export class CreateUserDto {
  @ApiProperty()
  @IsNotEmpty({ message: 'firstName empty' })
  firstName: string;

  @ApiProperty()
  @IsNotEmpty({ message: 'lastName empty' })
  lastName: string;

  @ApiProperty()
  @IsNotEmpty({ message: 'userAlias empty' })
  userAlias: string;

  @ApiProperty()
  @IsEmail()
  @IsNotEmpty({ message: 'email empty' })
  email: string;

  @ApiProperty()
  @IsNotEmpty({ message: 'phoneNumber empty' })
  phoneNumber: string;

  @ApiProperty()
  @IsNotEmpty({ message: 'password empty' })
  password: string;

  @ApiProperty()
  @IsNotEmpty({ message: 'cpf empty' })
  cpf: string;

  @ApiProperty()
  @IsNotEmpty({ message: 'isActive empty' })
  isActive: boolean;

  @ApiProperty()
  @IsNotEmpty({ message: 'roles are empty' })
  roles: [string];
}
