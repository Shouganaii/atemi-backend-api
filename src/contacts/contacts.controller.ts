import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Req,
  Request,
  UseGuards,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import {
  ApiTags,
  ApiNotFoundResponse,
  ApiBadRequestResponse,
  ApiInternalServerErrorResponse,
  ApiOkResponse,
} from '@nestjs/swagger';
import { DetailedException } from 'src/shared/exceptions';
import { JwtAuthGuard } from 'src/strategies/auth-guard';
import { ContactsService } from './contacts.service';
import {
  CreateContactsDTO,
  ReturnCreateContacts,
} from './entities/contact.entity';

@Controller('contacts')
@ApiTags('contacts')
@ApiNotFoundResponse({ description: 'Not Found', type: DetailedException })
@ApiBadRequestResponse({ description: 'Bad Request', type: DetailedException })
@ApiInternalServerErrorResponse({
  description: 'Internal Server Error',
  type: DetailedException,
})
export class ContactsController {
  constructor(
    private readonly contactsService: ContactsService,
    private jwtService: JwtService,
  ) {}

  @Post('')
  @UseGuards(JwtAuthGuard)
  @ApiOkResponse({ type: ReturnCreateContacts })
  create(@Body() createContactDto: CreateContactsDTO, @Req() req: Request) {
    const [, token] = req.headers['authorization'].split(' ');
    const user: any = this.jwtService.decode(token);
    return this.contactsService.create(createContactDto, user.payload._id);
  }

  // @Get()
  // @UseGuards(AdminGuard)
  // @ApiOkResponse({ type: [SearchAllContacts] })
  // findAll() {
  //   return this.contactsService.findAll();
  // }

  @Get('')
  @UseGuards(JwtAuthGuard)
  @ApiOkResponse({ type: ReturnCreateContacts })
  findByUser(@Req() req: Request) {
    const [, token] = req.headers['authorization'].split(' ');
    const user: any = this.jwtService.decode(token);
    return this.contactsService.findByUser(user.payload._id as string);
  }

  @Patch(':id_contact')
  @UseGuards(JwtAuthGuard)
  update(
    @Req() req: Request,
    @Param('id_contact') id: number,
    @Body() updateContactDto: CreateContactsDTO,
  ) {
    const [, token] = req.headers['authorization'].split(' ');
    const user: any = this.jwtService.decode(token);
    return this.contactsService.updateContact(
      id,
      user.payload._id as string,
      updateContactDto,
    );
  }

  @Delete(':id_contact')
  @UseGuards(JwtAuthGuard)
  remove(@Req() req: Request, @Param('id_contact') id: number) {
    const [, token] = req.headers['authorization'].split(' ');
    const user: any = this.jwtService.decode(token);
    return this.contactsService.remove(id, user.payload._id as string);
  }
}
