import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type SchedulesDocument = Schedules & Document;

@Schema()
export class Schedules {
  @Prop()
  user_id: string;

  @Prop()
  scheduled_to: Date;

  @Prop()
  description: string;

  @Prop()
  contact_id: string;

  @Prop()
  was_postponed: boolean;

  @Prop()
  is_canceled: boolean;

  @Prop()
  canceled_on: Date;

  @Prop({ required: true })
  notify_at: Date;

  @Prop()
  contacts: number[];

  @Prop()
  notify_contacts: boolean;

  @Prop()
  contacts_were_notified: boolean;

  @Prop()
  limit_date_to_cancel: Date;

  @Prop()
  was_notified: boolean;
}

export const SchedulesSchema = SchemaFactory.createForClass(Schedules);
