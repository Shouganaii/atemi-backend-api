import { CACHE_MANAGER, Inject, Injectable, Logger } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { Cache } from 'cache-manager';
@Injectable()
export class TasksService {
  private readonly logger = new Logger(TasksService.name);
  constructor(@Inject(CACHE_MANAGER) private cacheManager: Cache) {}
  @Cron(CronExpression.EVERY_10_MINUTES)
  async handleCron() {
    const cacheItems = await this.cacheManager.get('schedules');
    this.logger.log(cacheItems);
  }
}
