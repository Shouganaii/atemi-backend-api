import { ApiProperty } from '@nestjs/swagger';

export class FetchScheduleDto {
  @ApiProperty()
  user_id: string;

  @ApiProperty()
  scheduled_to: Date;

  @ApiProperty()
  description: string;

  @ApiProperty()
  contact_id: string;

  @ApiProperty()
  was_postponed: boolean;

  @ApiProperty()
  is_canceled: boolean;

  @ApiProperty()
  canceled_on: Date;

  @ApiProperty()
  notify_at: Date;

  @ApiProperty()
  contacts: [string];

  @ApiProperty()
  notify_contacts: boolean;

  @ApiProperty()
  contacts_were_notified: boolean;

  @ApiProperty()
  limit_date_to_cancel: Date;

  @ApiProperty()
  was_notified: boolean;
}
