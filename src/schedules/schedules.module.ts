import { Module, CacheModule } from '@nestjs/common';
import { SchedulesService } from './schedules.service';
import { SchedulesController } from './schedules.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Schedules, SchedulesSchema } from 'src/schemas/schedule.schema';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import { Users, UsersSchema } from 'src/schemas/user.schema';

@Module({
  controllers: [SchedulesController],
  providers: [SchedulesService],
  imports: [
    CacheModule.register({
      isGlobal: true,
    }),
    MongooseModule.forFeature([
      { name: Schedules.name, schema: SchedulesSchema },
      { name: Users.name, schema: UsersSchema },
    ]),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async () => ({
        secret: process.env.JWT_SECRET,
      }),
      inject: [ConfigService],
    }),
  ],
})
export class SchedulesModule {}
