import { Md5Crypto } from './crypto';

describe('Test crypto', () => {
  const sut = new Md5Crypto();

  it('Test crypto', () => {
    const valor1 = 'teste';
    const resultado1 = sut.incrypt(valor1);
    const valor2 = 'teste';
    const resultado2 = sut.incrypt(valor2);
    expect(resultado1).toEqual(resultado2);
  });
});
