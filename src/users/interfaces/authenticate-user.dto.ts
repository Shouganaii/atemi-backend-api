import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class AuthenticateUser {
  @ApiProperty({ example: 'user@user.com.br' || 'user' })
  @IsString({ message: 'Email ou nome de acesso do usuário são obrigatórios!' })
  user: string;

  @ApiProperty({ example: '123456' })
  @IsNotEmpty({ message: 'A senha não pode ficar vazia!' })
  password: string;
}
