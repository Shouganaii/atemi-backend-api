import { SchedulesDocument } from 'src/schemas/schedule.schema';
import { CreateScheduleDto } from './dto/create-schedule.dto';
import { UpdateScheduleDto } from './dto/update-schedule.dto';
import { ScheduleRepository } from './entities/schedule.interfaces';

export class SchedulesRepository implements ScheduleRepository {
  create(data: CreateScheduleDto): Promise<SchedulesDocument> {
    throw new Error('Method not implemented.');
  }
  findAll(): Promise<SchedulesDocument[]> {
    throw new Error('Method not implemented.');
  }
  findOne(id: string): Promise<SchedulesDocument[]> {
    throw new Error('Method not implemented.');
  }
  postponeSchedule(
    user_id: string,
    schedule_id: string,
    postpone_to: Date,
  ): Promise<any> {
    throw new Error('Method not implemented.');
  }
  cancelSchedule(user_id: string, schedule_id: string): Promise<any> {
    throw new Error('Method not implemented.');
  }
  postpone(
    schedule_id: string,
    user_id: string,
    updateScheduleDto: UpdateScheduleDto,
  ): Promise<any> {
    throw new Error('Method not implemented.');
  }
  cancel(user_id: string, schedule_id: string): Promise<any> {
    throw new Error('Method not implemented.');
  }
  remove(id: number): Promise<any> {
    throw new Error('Method not implemented.');
  }
}
