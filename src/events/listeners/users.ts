import { Injectable } from '@nestjs/common';
import { OnEvent } from '@nestjs/event-emitter';

//TODO mandar essas coisas pro zap dos admins
//TODO criar conexao com zap
//TODO criar feature toggle

@Injectable()
export class UsersEvents {
  @OnEvent('user.auth.success')
  handleAuth(event: any): void {
    console.log('usuario logado com sucesso');
    console.log(event);
  }
  @OnEvent('user.auth.inactive')
  handleAuthInactive(event: any): void {
    console.log('usuario inativo tentou se autenticar');
    console.log(event);
  }
  @OnEvent('user.auth.register')
  handleNewUser(event: any): void {
    console.log('usuario novo registrado');
    console.log(event);
  }
}
