import { SchedulesDocument } from 'src/schemas/schedule.schema';
import { CreateScheduleDto } from '../dto/create-schedule.dto';
import { UpdateScheduleDto } from '../dto/update-schedule.dto';

export interface ScheduleRepository {
  create(data: CreateScheduleDto): Promise<SchedulesDocument | void>;
  findAll(): Promise<SchedulesDocument[]>;
  findOne(id: string): Promise<SchedulesDocument[]>;
  postponeSchedule(
    user_id: string,
    schedule_id: string,
    postpone_to: Date,
  ): Promise<any>;
  cancelSchedule(user_id: string, schedule_id: string): Promise<any>;
  postpone(
    schedule_id: string,
    user_id: string,
    updateScheduleDto: UpdateScheduleDto,
  ): Promise<any>;
  cancel(user_id: string, schedule_id: string): Promise<any>;
  remove(id: number): Promise<any>;
}
