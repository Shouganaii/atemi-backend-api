import {
  BadRequestException,
  HttpException,
  HttpStatus,
  Injectable,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { isEmpty, set } from 'lodash';
import { Model } from 'mongoose';
import { Users, UsersDocument } from 'src/schemas/user.schema';
import { CreateContactDto } from './dto/create-contact.dto';
import { CreateContactsDTO } from './entities/contact.entity';

@Injectable()
export class ContactsService {
  constructor(
    @InjectModel(Users.name) private userModel: Model<UsersDocument>,
  ) {}
  async create(
    createContactDto: CreateContactDto,
    id: string,
  ): Promise<{ _id: string; contacts?: any[] }> {
    const user = await this.userModel.findById(id);

    if (isEmpty(user)) {
      throw new BadRequestException('User not found');
    }

    const { email, phone, name } = createContactDto;

    //TODO investigate why subdocument is not generating _id anymore
    console.log(user);
    if (isEmpty(user?.contacts)) {
      createContactDto._id = 1;
      user.contacts = [];
      user.contacts.push(createContactDto);
    } else {
      const data = {
        _id: user.contacts.length + 1,
        email,
        phone,
        name,
      };
      user.contacts.push(data);
    }

    await user.save();

    return { _id: user._id, contacts: user.contacts };
  }

  findAll() {
    //TODO investigar como paginar um subdocument
    return this.userModel.find().select({
      contacts: 1,
      firstName: 1,
      lastName: 1,
      _id: 1,
    });
  }

  async findByUser(id: string) {
    return await this.userModel.findById(id).select({ contacts: 1 });
  }

  async updateContact(
    id: number,
    idUser: string,
    contactInfo: CreateContactsDTO,
  ): Promise<{ _id: string; contacts?: any[] }> {
    //TODO melhorar essa querie
    const user = await this.userModel.findById(idUser);

    if (isEmpty(user))
      throw new HttpException('User does not exist', HttpStatus.NOT_FOUND);
    //TODO adicionar condição de softdelete
    const contactIndex = user.contacts
      .map((contact) => contact._id)
      .indexOf(Number(id));

    //TODO aqui o contactIndex pode ser 0
    const dontHasContactIndex =
      contactIndex === undefined || contactIndex == -1;

    if (dontHasContactIndex)
      throw new HttpException('Contact does not exist', HttpStatus.NOT_FOUND);

    user.contacts[contactIndex].email =
      contactInfo.email || user.contacts[contactIndex].email;
    user.contacts[contactIndex].phone =
      contactInfo.phone || user.contacts[contactIndex].phone;
    user.contacts[contactIndex].name =
      contactInfo.name || user.contacts[contactIndex].name;

    await user.save();

    return { _id: user._id, contacts: user.contacts };
  }

  async remove(
    id: number,
    id_user: string,
  ): Promise<{ _id: string; contacts?: any[] }> {
    //TODO melhorar essa querie
    const user = await this.userModel.findOne({ _id: id_user });

    if (isEmpty(user))
      throw new HttpException('User does not exist', HttpStatus.NOT_FOUND);

    const contactIndex = user.contacts
      .map((contact) => contact._id)
      .indexOf(Number(id));

    const dontHasContactIndex =
      contactIndex === undefined || contactIndex == -1;

    if (dontHasContactIndex)
      throw new HttpException('Contact does not exist', HttpStatus.NOT_FOUND);

    const newContacts = user.contacts;

    delete newContacts[contactIndex];
    //TODO aplicar softdelete
    const filteredContacts = newContacts.filter((el) => {
      return el !== null && typeof el !== 'undefined';
    });

    user.contacts = filteredContacts;

    await user.save();

    return { _id: user._id, contacts: user.contacts };
  }
}
