import { ApiProperty } from '@nestjs/swagger';

class Contacts {
  @ApiProperty()
  email: string;

  @ApiProperty()
  phone: string;

  @ApiProperty()
  name: string;
}

class Geoloc {
  @ApiProperty()
  lat: number;

  @ApiProperty()
  lon: number;
}

class Address {
  @ApiProperty()
  street: string;

  @ApiProperty()
  neighbourhood: string;

  @ApiProperty()
  zipCode: string;

  @ApiProperty()
  publicPlace: string;

  @ApiProperty()
  addressNumber: string;

  @ApiProperty()
  location: string;

  @ApiProperty()
  uf: string;

  @ApiProperty()
  geoloc: Geoloc;
}
export class User {
  @ApiProperty()
  firstName: string;

  @ApiProperty()
  userAlias: string;

  @ApiProperty()
  lastName: string;

  @ApiProperty()
  email: string;

  @ApiProperty()
  password: string;

  @ApiProperty()
  cpf: string;

  @ApiProperty()
  isActive: boolean;

  @ApiProperty()
  roles: [string];

  @ApiProperty()
  contacts: Contacts[];

  @ApiProperty()
  address: Address;

  @ApiProperty()
  createdAt: Date;
}
