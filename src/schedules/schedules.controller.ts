import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Req,
  Request,
  UseGuards,
} from '@nestjs/common';
import { SchedulesService } from './schedules.service';
import { CreateScheduleDto } from './dto/create-schedule.dto';
import { UpdateScheduleDto } from './dto/update-schedule.dto';
import {
  ApiBadRequestResponse,
  ApiInternalServerErrorResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiTags,
} from '@nestjs/swagger';
import { FetchScheduleDto } from './dto/fetch-schedule.dto';
import { DetailedException } from 'src/shared/exceptions';
import { JwtService } from '@nestjs/jwt';
import { JwtAuthGuard } from 'src/strategies/auth-guard';
@Controller('schedules')
@ApiTags('schedules')
@ApiNotFoundResponse({ description: 'Not Found', type: DetailedException })
@ApiBadRequestResponse({ description: 'Bad Request', type: DetailedException })
@ApiInternalServerErrorResponse({
  description: 'Internal Server Error',
  type: DetailedException,
})
export class SchedulesController {
  constructor(
    private readonly schedulesService: SchedulesService,
    private jwtService: JwtService,
  ) {}

  @Post()
  @UseGuards(JwtAuthGuard)
  @ApiOkResponse({ type: CreateScheduleDto })
  create(@Body() createScheduleDto: CreateScheduleDto) {
    return this.schedulesService.create(createScheduleDto);
  }

  //TODO liberar só para admin
  @Get()
  @UseGuards(JwtAuthGuard)
  @ApiOkResponse({ type: [FetchScheduleDto] })
  async findAll() {
    return await this.schedulesService.findAll();
  }

  @Get('/mine')
  @UseGuards(JwtAuthGuard)
  @ApiOkResponse({ type: [FetchScheduleDto] })
  findOne(@Req() req: Request) {
    const [, token] = req.headers['authorization'].split(' ');
    const user: any = this.jwtService.decode(token);
    return this.schedulesService.findOne(user.payload._id as string);
  }

  @Patch('/postpone/:schedule_id')
  @UseGuards(JwtAuthGuard)
  @ApiOkResponse({ type: UpdateScheduleDto })
  postpone(
    @Param('schedule_id') id: string,
    @Body() updateScheduleDto: UpdateScheduleDto,
    @Req() req: Request,
  ) {
    const [, token] = req.headers['authorization'].split(' ');
    const user: any = this.jwtService.decode(token);
    return this.schedulesService.postpone(
      id,
      user.payload._id as string,
      updateScheduleDto,
    );
  }

  @Patch('/cancel/:schedule_id')
  @UseGuards(JwtAuthGuard)
  cancel(@Param('schedule_id') id: string, @Req() req: Request) {
    const [, token] = req.headers['authorization'].split(' ');
    const user: any = this.jwtService.decode(token);
    return this.schedulesService.cancel(user.payload._id as string, id);
  }
}
