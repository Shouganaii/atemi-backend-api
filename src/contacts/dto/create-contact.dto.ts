import { Prop } from '@nestjs/mongoose';
import { Contacts } from '../entities/contact.entity';
//TODO por validações aqui
export class CreateContactDto {
  @Prop()
  _id?: number;

  @Prop()
  email: string;

  @Prop()
  phone: string;

  @Prop()
  name: string;
}
