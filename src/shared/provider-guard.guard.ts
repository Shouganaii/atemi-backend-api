import {
  SetMetadata,
  Injectable,
  CanActivate,
  ExecutionContext,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { JwtService } from '@nestjs/jwt';
export enum Role {
  CUSTOMER = 'customer',
  ADMIN = 'admin',
}

export const ROLES_KEY = 'roles';
export const Roles = (...roles: Role[]) => SetMetadata(ROLES_KEY, roles);

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(private reflector: Reflector, private jwtService: JwtService) {}

  canActivate(context: ExecutionContext): boolean {
    const requiredRoles = this.reflector.getAllAndOverride<Role[]>(ROLES_KEY, [
      context.getHandler(),
      context.getClass(),
    ]);

    if (!requiredRoles) {
      return true;
    }

    const {
      headers: { authorization },
    } = context.switchToHttp().getRequest();

    const [, token] = authorization.split(' ');

    const user: any = this.jwtService.decode(token);

    return requiredRoles.some((role) => user.payload.roles?.includes(role));
  }
}

@Injectable()
export class AdminGuard implements CanActivate {
  constructor(private reflector: Reflector, private jwtService: JwtService) {}

  canActivate(context: ExecutionContext): boolean {
    const {
      headers: { authorization },
    } = context.switchToHttp().getRequest();

    if (!authorization)
      throw new HttpException('Unauthorized', HttpStatus.FORBIDDEN);

    const [, token] = authorization.split(' ');

    const user: any = this.jwtService.decode(token);

    return user.payload.roles?.includes(Role.ADMIN);
  }
}
