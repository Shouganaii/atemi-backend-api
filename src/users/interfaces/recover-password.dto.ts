import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class RecoverPassword {
  @ApiProperty({ example: 'user@user.com.br' || 'user' })
  @IsString({ message: 'Email ou nome de acesso do usuário são obrigatórios!' })
  email: string;
}
